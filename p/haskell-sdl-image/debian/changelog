haskell-sdl-image (0.6.2.0-4) unstable; urgency=medium

  * Add a missing Provides field for the dev/prof packages.

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Mon, 04 Jul 2022 18:59:29 +0300

haskell-sdl-image (0.6.2.0-3) unstable; urgency=medium

  * Declare compliance with Debian policy 4.6.1
  * Sourceful upload for GHC 9.0.2

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sat, 02 Jul 2022 17:16:08 +0300

haskell-sdl-image (0.6.2.0-2) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Fix "Upstream-Name" field in "debian/copyright" to match package
    name on hackage.

  [ Ilias Tsitsimpis ]
  * Sourceful upload for GHC 8.8

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Wed, 17 Jun 2020 11:23:15 +0300

haskell-sdl-image (0.6.2.0-1) unstable; urgency=medium

  * Bump debhelper compat level to 10
  * New upstream release

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sat, 29 Sep 2018 17:32:49 +0300

haskell-sdl-image (0.6.1.2-1) unstable; urgency=medium

  * Set Rules-Requires-Root to no.
  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 03 Jul 2018 08:36:28 -0400

haskell-sdl-image (0.6.1-12) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:05:03 -0400

haskell-sdl-image (0.6.1-11) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:35:58 -0400

haskell-sdl-image (0.6.1-10) experimental; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Mon, 17 Oct 2016 11:48:50 -0400

haskell-sdl-image (0.6.1-9) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:55:02 -0500

haskell-sdl-image (0.6.1-8) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:28:38 +0200

haskell-sdl-image (0.6.1-7) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:53:13 +0200

haskell-sdl-image (0.6.1-6) experimental; urgency=low

  * Adjust watch file to new hackage layout
  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:12:01 +0100

haskell-sdl-image (0.6.1-5) unstable; urgency=low

  * Enable compat level 9

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:51:56 +0200

haskell-sdl-image (0.6.1-4) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change

 -- Joachim Breitner <nomeata@debian.org>  Thu, 18 Oct 2012 10:26:07 +0200

haskell-sdl-image (0.6.1-3) unstable; urgency=low

  * Sourceful upload to rebuild documentation package

 -- Iain Lane <laney@debian.org>  Mon, 27 Feb 2012 21:01:46 +0000

haskell-sdl-image (0.6.1-2) unstable; urgency=low

  [ Marco Silva ]
  * Use ghc instead of ghc6

 -- Joachim Breitner <nomeata@debian.org>  Tue, 24 May 2011 15:58:00 +0200

haskell-sdl-image (0.6.1-1) unstable; urgency=low

  [ Joachim Breitner ]
  * Provide libsdl-image1.2-dev dependency (Closes: #579276)

  [ Erik de Castro Lopo ]
  * New upstream.

 -- Erik de Castro Lopo <erikd@mega-nerd.com>  Wed, 23 Jun 2010 17:50:43 +1000

haskell-sdl-image (0.5.2-1) unstable; urgency=low

  [ Miriam Ruiz ]
  * Initial release. Closes: #572038.
    Based on a package by Christoph Korn <christoph.korn@getdeb.net>

  [ Erik de Castro Lopo ]
  * Use debian/compat 7.
  * debian/source/format: Use 3.0 (quilt).
  * debian/control
    - Standards version 3.8.4.
    - Use Build-Depends-Indep.
    - Use Vcs-Browser.
    - Use more synthetic Vcs-Darcs.
    - Use all haskell variables.
    - Maintainer is Debian Haskell Group, Miriam Ruiz moved to Uploaders.

 -- Erik de Castro Lopo <erikd@mega-nerd.com>  Mon,  1 Mar 2010 20:31:42 +1100
