Source: haskell-data-tree-print
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Clint Adams <clint@debian.org>
Priority: optional
Section: haskell
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.13),
 cdbs,
 ghc,
 ghc-prof,
 libghc-syb-dev (>= 0.6),
 libghc-syb-dev (<< 0.8),
 libghc-syb-prof (>= 0.6),
 libghc-syb-prof (<< 0.8),
Build-Depends-Indep: ghc-doc,
 libghc-syb-doc,
Standards-Version: 4.6.1
Homepage: https://github.com/lspitzner/data-tree-print
X-Description: print Data instances as a nested tree
 Provides functionality similar to that of the Show class: Taking
 some arbitrary value and returning a String.
 .
  * Output is not intended to be valid Haskell
  * Requires a Data.Data.Data instance instead of a Text.Show one
  * Output, if large, is often easier to parse than `show` output
    due to the formatting as a nested tree
  * The user can adapt the behaviour at runtime using a custom
    layout expressed via syb-style extension

Package: libghc-data-tree-print-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-data-tree-print-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-data-tree-print-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
