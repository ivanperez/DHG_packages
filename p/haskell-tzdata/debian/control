Source: haskell-tzdata
Maintainer: Debian Haskell Group <pkg-haskell-maintainers@lists.alioth.debian.org>
Uploaders: Robert Greener <rob@robgreener.com>
Priority: optional
Section: haskell
Rules-Requires-Root: no
Build-Depends: debhelper (>= 10),
 haskell-devscripts-minimal | haskell-devscripts (>= 0.13),
 cdbs,
 ghc,
 ghc-prof,
 libghc-vector-dev (>= 0.9),
 libghc-vector-dev (<< 0.13),
 libghc-vector-prof,
 libghc-hunit-dev (>= 1.2),
 libghc-hunit-dev (<< 1.7),
 libghc-hunit-prof,
 libghc-tasty-dev,
 libghc-tasty-prof,
 libghc-tasty-hunit-dev,
 libghc-tasty-hunit-prof,
 libghc-tasty-th-dev,
 libghc-tasty-th-prof,
 tzdata,
Build-Depends-Indep: ghc-doc,
 libghc-vector-doc,
Standards-Version: 4.6.1
Homepage: https://github.com/ysangkok/haskell-tzdata
Vcs-Browser: https://salsa.debian.org/haskell-team/DHG_packages/tree/master/p/haskell-tzdata
Vcs-Git: https://salsa.debian.org/haskell-team/DHG_packages.git [p/haskell-tzdata]
X-Description: Time zone database (as files and as a module)
 The goal of this package is to distribute the standard Time Zone
 Database in a cabal package, so that it can be used in Haskell
 programs uniformly on all platforms.
 .
 This package currently ships the @2019c@ version of the time zone
 database.  The version of the time zone database shipped is always
 reflected in the version of this package: @x.y.YYYYMMDD.z@, then
 @YYYYMMDD@ is the official release date of time zone database.
 .
 See: <http://www.iana.org/time-zones> for more info about the time
 zone database.
 .
 See also the @tz@ package <http://hackage.haskell.org/package/tz> or
 the @timezone-olson@ and @timezone-series@ packages that provide
 facilities to /use/ the data shipped here. (The @tz@ package
 automatically installs this package.)

Package: libghc-tzdata-dev
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
 ${shlibs:Depends},
 tzdata,
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-tzdata-prof
Architecture: any
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Provides: ${haskell:Provides},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}

Package: libghc-tzdata-doc
Architecture: all
Section: doc
Depends: ${haskell:Depends},
 ${misc:Depends},
Recommends: ${haskell:Recommends},
Suggests: ${haskell:Suggests},
Conflicts: ${haskell:Conflicts},
Description: ${haskell:ShortDescription}${haskell:ShortBlurb}
 ${haskell:LongDescription}
 .
 ${haskell:Blurb}
