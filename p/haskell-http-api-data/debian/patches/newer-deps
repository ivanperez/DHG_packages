Index: b/http-api-data.cabal
===================================================================
--- a/http-api-data.cabal
+++ b/http-api-data.cabal
@@ -1,6 +1,7 @@
 cabal-version:   >= 1.10
 name:            http-api-data
 version:         0.4.3
+x-revision:      6
 
 synopsis:        Converting to/from HTTP API data like URL pieces, headers and query parameters.
 category:        Web
@@ -31,7 +32,9 @@ tested-with:
   GHC==8.4.4,
   GHC==8.6.5,
   GHC==8.8.4,
-  GHC==8.10.3
+  GHC==8.10.7,
+  GHC==9.0.1,
+  GHC==9.2.1
 
 flag use-text-show
   description: Use text-show library for efficient ToHttpApiData implementations.
@@ -43,11 +46,11 @@ library
     include-dirs:   include/
 
     -- GHC bundled
-    build-depends:   base                  >= 4.7      && < 4.15
+    build-depends:   base                  >= 4.7      && < 4.17
                    , bytestring            >= 0.10.4.0 && < 0.12
                    , containers            >= 0.5.5.1  && < 0.7
-                   , text                  >= 1.2.3.0  && < 1.3
-                   , transformers          >= 0.3      && < 0.6
+                   , text                  >= 1.2.3.0  && < 1.3 || >=2.0 && <2.1
+                   , transformers          >= 0.3      && < 0.7
 
     -- so Semigroup Builder exists
     if impl(ghc >= 8.0)
@@ -55,11 +58,11 @@ library
 
     -- other-dependencies
     build-depends:
-                     attoparsec            >= 0.13.2.2 && < 0.14
+                     attoparsec            >= 0.13.2.2 && < 0.15
                    , attoparsec-iso8601    >= 1.0.2.0  && < 1.1
-                   , base-compat           >= 0.10.5   && < 0.12
+                   , base-compat           >= 0.10.5   && < 0.13
                    , cookie                >= 0.4.3    && < 0.5
-                   , hashable              >= 1.2.7.0  && < 1.4
+                   , hashable              >= 1.2.7.0  && < 1.5
                    , http-types            >= 0.12.3   && < 0.13
                    , tagged                >= 0.8.5    && < 0.9
                    , time-compat           >= 1.9.5    && < 1.10
@@ -72,11 +75,11 @@ library
         void >= 0.7.3 && < 0.8
 
     if !impl(ghc >= 8.0)
-      build-depends: semigroups            >= 0.18.5   && < 0.20
+      build-depends: semigroups            >= 0.18.5   && < 0.21
 
     if flag(use-text-show)
       cpp-options: -DUSE_TEXT_SHOW
-      build-depends: text-show        >= 3.8.2 && <3.9
+      build-depends: text-show        >= 3.8.2 && <3.10
 
     exposed-modules:
       Web.HttpApiData
@@ -96,7 +99,7 @@ test-suite spec
     hs-source-dirs: test
     ghc-options:   -Wall
     default-language: Haskell2010
-    build-tool-depends: hspec-discover:hspec-discover >= 2.7.1 && <2.8
+    build-tool-depends: hspec-discover:hspec-discover >= 2.7.1 && <2.10
     -- inherited  depndencies
     build-depends:
                      base
@@ -113,7 +116,7 @@ test-suite spec
       build-depends: nats
 
     build-depends:   HUnit                >= 1.6.0.0  && <1.7
-                   , hspec                >= 2.7.1    && <2.8
+                   , hspec                >= 2.7.1    && <2.10
                    , QuickCheck           >= 2.13.1   && <2.15
                    , quickcheck-instances >= 0.3.25.2 && <0.4
 
