haskell-ixset (1.1.1.2-1) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Declare compliance with Debian policy 4.6.1

  [ Clint Adams ]
  * New upstream release

 -- Clint Adams <clint@debian.org>  Thu, 30 Jun 2022 12:30:02 -0400

haskell-ixset (1.1.1.1-2) unstable; urgency=medium

  * Sourceful upload for GHC 8.8

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Thu, 18 Jun 2020 14:55:11 +0300

haskell-ixset (1.1.1.1-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Fix "Upstream-Name" field in "debian/copyright" to match package
    name on hackage.

  [ Clint Adams ]
  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 18 Feb 2020 22:15:38 -0500

haskell-ixset (1.1.1-1) unstable; urgency=medium

  [ Clint Adams ]
  * Set Rules-Requires-Root to no.

  [ Ilias Tsitsimpis ]
  * Bump debhelper compat level to 10
  * New upstream release

 -- Ilias Tsitsimpis <iliastsi@debian.org>  Sun, 30 Sep 2018 12:13:26 +0300

haskell-ixset (1.0.7-4) unstable; urgency=medium

  [ Ilias Tsitsimpis ]
  * Change Priority to optional. Since Debian Policy version 4.0.1,
    priority extra has been deprecated.
  * Use the HTTPS form of the copyright-format URL
  * Modify d/watch and Source field in d/copyright to use HTTPS
  * Declare compliance with Debian policy 4.1.1
  * Use salsa.debian.org URLs in Vcs-{Browser,Git} fields

  [ Clint Adams ]
  * Bump to Standards-Version 4.1.4.

 -- Clint Adams <clint@debian.org>  Mon, 09 Apr 2018 20:04:54 -0400

haskell-ixset (1.0.7-3) unstable; urgency=medium

  * Upload to unstable as part of GHC 8 transition.

 -- Clint Adams <clint@debian.org>  Thu, 27 Oct 2016 18:34:35 -0400

haskell-ixset (1.0.7-2) experimental; urgency=medium

  * Temporarily build-depend on ghc 8.

 -- Clint Adams <clint@debian.org>  Sun, 16 Oct 2016 20:01:39 -0400

haskell-ixset (1.0.7-1) unstable; urgency=medium

  [ Dmitry Bogatov ]
  * Use secure (https) uri in Vcs-Git field in 'debian/control'
  * Bump standards version to 3.9.8 (no changes needed)

  [ Clint Adams ]
  * New upstream release

 -- Clint Adams <clint@debian.org>  Tue, 12 Jul 2016 16:05:51 -0400

haskell-ixset (1.0.6.1-1) unstable; urgency=medium

  * New upstream release

 -- Clint Adams <clint@debian.org>  Sun, 10 Jan 2016 22:15:38 -0500

haskell-ixset (1.0.6-4) unstable; urgency=medium

  * Switch Vcs-Git/Vcs-Browser headers to new location.

 -- Clint Adams <clint@debian.org>  Thu, 03 Dec 2015 14:54:40 -0500

haskell-ixset (1.0.6-3) experimental; urgency=medium

  * Bump standards-version to 3.9.6
  * Depend on haskell-devscripts >= 0.10 to ensure that this package
    builds against GHC in experimental

 -- Joachim Breitner <nomeata@debian.org>  Thu, 20 Aug 2015 10:28:01 +0200

haskell-ixset (1.0.6-2) unstable; urgency=medium

  * Upload to unstable

 -- Joachim Breitner <nomeata@debian.org>  Mon, 27 Apr 2015 11:51:09 +0200

haskell-ixset (1.0.6-1) experimental; urgency=medium

  * New upstream release

 -- Joachim Breitner <nomeata@debian.org>  Mon, 06 Apr 2015 13:05:36 +0200

haskell-ixset (1.0.5-2) experimental; urgency=medium

  * Depend on haskell-devscripts 0.9, found in experimental

 -- Joachim Breitner <nomeata@debian.org>  Sat, 20 Dec 2014 17:11:10 +0100

haskell-ixset (1.0.5-1) unstable; urgency=low

  [ Joachim Breitner ]
  * Adjust watch file to new hackage layout

  [ Clint Adams ]
  * New upstream version.

 -- Clint Adams <clint@debian.org>  Sun, 08 Dec 2013 11:04:48 -0500

haskell-ixset (1.0.3-4) unstable; urgency=low

  * Enable compat level 9
  * Use substvars for Haskell description blurbs

 -- Joachim Breitner <nomeata@debian.org>  Fri, 24 May 2013 12:51:12 +0200

haskell-ixset (1.0.3-3) experimental; urgency=low

  * Depend on haskell-devscripts 0.8.13 to ensure this packages is built
    against experimental
  * Bump standards version, no change

 -- Joachim Breitner <nomeata@debian.org>  Thu, 18 Oct 2012 10:25:44 +0200

haskell-ixset (1.0.3-2) unstable; urgency=low

  * Initial release (closes: #673624).

 -- Giovanni Mascellani <gio@debian.org>  Sun, 20 May 2012 12:33:21 +0200
